# pytest_finder

`pytest` is very useful, but parameterized tests are a bit annoying to execute individually:  `pytest` won't always choose obvious names for the parametrization, if a user changes the value of parameter then the name `pytest` expects will change and so shell history is no use etc. etc.

The aim of this little script is to use `pytest`'s test collection logic, in conjunction with the awesome fzf tool, to make finding and executing python tests quicker and less painful.

## Installation
- [ ] It would be nice if this was installable to avoid users having to setup shall aliases or symlinks to get it to work
- [ ] Make this thing pip installable?

## Usage

![Simple use-case gif](./usage.gif)

The simplest case is as follows (assumed to be running in the same directory `pytest` would normally be invoked from - see the `--pytest-directory` option if this assumption does not hold)

```bash
python path/to/pytest_finder.py
```

`pytest_finder` supports three commandline options.

The `--pytest-directory` command line argument allows the user to specify which test directory to search in.  The default is `'.'` (i.e. the present working directory)

The `--test-filter` option can be used to pre-filter the test files `fzf` will return to the user.  The filter can still be changed while `pytest_finder` is running.

The `--pytest-options` argument forwards on to `pytest` and testing parameters the user requires.  For example `--pytest-options=xvvvv` would result in maximally verbose logging output and would terminate after the first failed test is encountered
