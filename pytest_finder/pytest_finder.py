#!/usr/bin/env python3

import os
import sys
import logging
import pytest
import time
import argparse
from typing import List, Iterable, Optional
from collections import defaultdict

from iterfzf import iterfzf

logging.basicConfig(level=logging.INFO)
LOGGER = logging.getLogger()

ALL_TESTS = "all tests"


class LogSwallower:
    def __init__(self):
        self.old_out = sys.stdout
        self.old_err = sys.stderr

    def __enter__(self):
        sys.stdout = open(os.devnull, "w")
        sys.stderr = open(os.devnull, "w")

    def __exit__(self, *args):
        sys.stdout = self.old_out
        sys.stderr = self.old_err


class TestCollector:
    def __init__(self, pytest_directory: str, pytest_options: str, test_filter: str):
        self.pytest_directory = pytest_directory
        self.pytest_options = pytest_options if pytest_options else ""
        self.test_filter = test_filter
        self._test_files = set()
        self._tests_in_files = defaultdict(set)
        self._test_parametrizations = defaultdict(set)

    def pytest_collection_modifyitems(self, items) -> None:
        start_s = time.time()
        LOGGER.debug(f"pytest collector found {len(items)} items")
        for item in items:
            LOGGER.debug(f"Pytest collector item: {item}")
            test_path, _, test_name_with_fixtures = item.location
            test_name = item.function.__name__
            self._test_files.add(test_path)
            self._tests_in_files[test_path].add(ALL_TESTS)
            self._tests_in_files[test_path].add(test_name)
            self._test_parametrizations[test_path, test_name].add(test_name_with_fixtures)
        LOGGER.debug(f"pytest collection took {time.time() - start_s:.2f}s")

    @staticmethod
    def _iterate(iterable: Iterable[str]) -> Iterable[str]:
        for f in iterable:
            yield f

    def _iterate_files(self) -> Iterable[str]:
        return self._iterate(sorted(self._test_files))

    def _iterate_tests(self, file_: str) -> Iterable[str]:
        return self._iterate(sorted(self._tests_in_files[file_]))

    def _iterate_parametrizations(self, file_: str, test_name: str) -> Iterable[str]:
        return self._iterate(sorted(self._test_parametrizations[file_, test_name]))

    def _collect_tests(self) -> None:
        # pytest writes a lot of unnecessary logs which we capture for neatness
        with LogSwallower():
            pytest.main(["--collect-only", self.pytest_directory], plugins=[self])

    @staticmethod
    def build_pytest_args(path: str, parametrization: Optional[str] = None, options: Optional[str] = None) -> List[str]:
        # Construct the test path according to pytests expectations
        path = f"{path}::{parametrization}" if parametrization else path
        args = [path]
        if options:
            args = ["-" + options, *args]
        return args

    def _yield_results(self) -> None:
        # First, present available test files and await selection
        test_path = iterfzf(self._iterate_files(), query=self.test_filter)

        # Next select the test:
        test_name = iterfzf(self._iterate_tests(test_path))
        if test_name == ALL_TESTS:
            return pytest.main(args=self.build_pytest_args(test_path))

        # Lastly, choose a parametrization
        params = self._test_parametrizations[test_path, test_name]
        test_parametrization = params.pop() if len(params) == 1 else iterfzf(params)

        # Execute the pytest command!
        pytest.main(args=self.build_pytest_args(test_path, test_parametrization))

    def run(self) -> None:
        self._collect_tests()
        self._yield_results()


def collect_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description="pytest collector")
    parser.add_argument("-d", "--pytest-directory", help="Relative path to tests", default=".")
    parser.add_argument("-o", "--pytest-options", help='pytest runner arguments e.g. "xsv"', default="")
    parser.add_argument("-f", "--test-filter", help="pytest name filter", default="")
    return parser.parse_args()


def main():
    args = collect_args()
    TestCollector(args.pytest_directory, args.pytest_options, args.test_filter).run()


if __name__ == "__main__":
    main()
